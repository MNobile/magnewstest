import com.app.db.DBManager;
import com.app.db.model.UserModel;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class MinimalTest {

    @Test
    public void test(){
        UserModel user = new UserModel(-1, "test", "test");
        try {
            Assert.assertTrue(DBManager.getInstance().getQueryDurationNanos(DBManager.QueryType.INSERT, user, true) > 0);

            Assert.assertTrue(DBManager.getInstance().getQueryDurationNanos(DBManager.QueryType.SELECT, user,false) > 0);
        } catch (SQLException e) {
            Assert.assertTrue(false);
        }
    }
}
