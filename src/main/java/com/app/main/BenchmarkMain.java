package com.app.main;

import com.app.benchmarkcontrol.BenchmarkController;
import com.app.benchmarkcontrol.BenchmarkResult;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BenchmarkMain {

    public static void main(String[] args) {

        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("./src/main/resources/configuration.properties");
            prop.load(input);
            BenchmarkController bmController = new BenchmarkController(Integer.parseInt(prop.getProperty("benchmarkIterations")), Integer.parseInt(prop.getProperty("commitAfter")));
            BenchmarkResult selectResult, insertResult;


            selectResult = bmController.benchmarkUsersSelect();
            insertResult = bmController.benchmarkUsersInsert();

            System.out.println("SELECTS - " + selectResult);
            System.out.println("INSERTS - " + insertResult);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




}
