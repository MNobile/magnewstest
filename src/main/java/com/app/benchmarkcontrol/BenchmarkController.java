package com.app.benchmarkcontrol;

import com.app.db.DBManager;
import com.app.db.model.UserModel;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class BenchmarkController {


    private int iterations;
    private int commitAfter;

    public BenchmarkController(int iterations, int commitAfter) {
        this.iterations = iterations;
        this.commitAfter = commitAfter;
    }

    public BenchmarkResult benchmarkUsersSelect(){
        AtomicInteger failedQueries = new AtomicInteger();
        BenchmarkResult result;
        DBManager dbManager = DBManager.getInstance();
        List<UserModel> users = new ArrayList<>();

        for (int i = 1; i <= iterations; i++) {
            users.add(new UserModel(i, "user"+i, "user"+i));
        }

        List<Long> times = users.stream()
                .map(userModel -> {
                    try {
                        return dbManager.getQueryDurationNanos(DBManager.QueryType.SELECT, userModel, false);
                    } catch (SQLException e) {
                        failedQueries.getAndIncrement();
                        return null;
                    }
                }).collect(Collectors.toList());

        times.removeAll(Collections.singleton(null));

        try {
            long min = times.stream().min(Long::compareTo).get();
            long max = times.stream().max(Long::compareTo).get();
            long avg = (times.stream().reduce((aLong, aLong2) -> aLong + aLong2).get()) / times.size();
            result = new BenchmarkResult(min, max, avg, failedQueries.get());
        } catch (NoSuchElementException e){
            result = null;
        }

        return result;
    }

    public BenchmarkResult benchmarkUsersInsert(){
        AtomicInteger failedQueries = new AtomicInteger();
        BenchmarkResult result;
        DBManager dbManager = DBManager.getInstance();
        List<UserModel> users = new ArrayList<>();

        for (int i = 1; i <= iterations; i++) {
            users.add(new UserModel(i, "user"+i, "user"+i));
        }

        List<Long> times = users.stream()
                .map(userModel -> {
                    try {
                        //HANDLING THE COMMIT
                        if(userModel.getId() % commitAfter == 0 || userModel.getId() == iterations){
                            return dbManager.getQueryDurationNanos(DBManager.QueryType.INSERT, userModel, true);
                        } else {
                            return dbManager.getQueryDurationNanos(DBManager.QueryType.INSERT, userModel, false);
                        }
                    } catch (SQLException e) {
                        failedQueries.getAndIncrement();
                        return null;
                    }
                }).collect(Collectors.toList());

        times.removeAll(Collections.singleton(null));

        try {
            long min = times.stream().min(Long::compareTo).get();
            long max = times.stream().max(Long::compareTo).get();
            long avg = (times.stream().reduce((aLong, aLong2) -> aLong + aLong2).get()) / times.size();
            result = new BenchmarkResult(min, max, avg, failedQueries.get());
        } catch (NoSuchElementException e){
            result = null;
        }

        return result;
    }
}
