package com.app.benchmarkcontrol;

public class BenchmarkResult {

    private long min;
    private long max;
    private long avg;
    private int failedQueries;

    public BenchmarkResult(long min, long max, long avg, int failedQueries) {
        this.min = min;
        this.max = max;
        this.avg = avg;
        this.failedQueries = failedQueries;
    }

    public long getMin() {
        return min;
    }

    public long getMax() {
        return max;
    }

    public long getAvg() {
        return avg;
    }

    public int getFailedQueries() {
        return failedQueries;
    }

    @Override
    public String toString() {
        return "BENCHMARK RESULT (milliseconds)  MIN: " + (double)this.min/1000000 +
                "  |  MAX: " +(double)this.max/1000000 +
                "  |  AVG: " +(double)this.avg/1000000 +
                "  |  FAILED: " +this.failedQueries;
    }
}
