package com.app.db;


import com.app.db.model.UserModel;
import com.app.exceptions.DBConnectionFailedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBManager {
    private static DBManager ourInstance = new DBManager();
    private Connection con;

    public static DBManager getInstance() {
        return ourInstance;
    }

    private DBManager() {
        try {

            //CONNECT TO DB
            con = getConnection();

            //CREATE THE USERS TABLE
            PreparedStatement pst = con.prepareStatement(new String(Files.readAllBytes(Paths.get("./src/main/resources/dbInit.sql"))));
            pst.execute();
        } catch (DBConnectionFailedException | IOException | SQLException ex) {
            Logger lgr = Logger.getLogger(DBManager.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public long getQueryDurationNanos(QueryType type, Object model, boolean commit) throws SQLException {
        long start = 0;
        long end = 0;

        PreparedStatement pst = null;
        switch (type) {
            case INSERT:
                pst = con.prepareStatement(buildInsertQueryByModel(model));
                break;

            case SELECT:
                pst = con.prepareStatement(buildSelectQueryByModel(model));
                break;
        }
        if (pst != null) {
            start = System.nanoTime();
            pst.execute();
            end = System.nanoTime();
        } else {
            throw new SQLException();
        }
        if (commit) {
            con.commit();
        }

        return end - start;
    }

    /**
     * Method to build a query string to select a single record corresponding to the model in input. Useful only in tests or benchmark
     * scenarios.
     */
    private static String buildSelectQueryByModel(Object model) {

        String query = null;

        //GIVEN A MODEL, BUILD THE SELECT

        if (model instanceof UserModel) {
            query = "SELECT USERNAME FROM USERS WHERE ID = " + ((UserModel) model).getId();
        }
        //extend with eventually others models.

        return query;
    }

    /**
     * Method to build a query string to insert a single record corresponding to the model in input. Useful only in tests or benchmark
     * scenarios.
     */
    private static String buildInsertQueryByModel(Object model) {

        String query = null;

        //GIVEN A MODEL, BUILD THE SELECT

        if (model instanceof UserModel) {
            query = "INSERT INTO USERS (ID, USERNAME, PASSWORD) VALUES (" + ((UserModel) model).getId() + "," +
                    " \'" + ((UserModel) model).getUsername() + "\'," +
                    " \'" + ((UserModel) model).getPassword() + "\')";
            //System.out.println(query);
        }
        //extend with eventually others models.

        return query;
    }

    private static Connection getConnection() throws DBConnectionFailedException {

        Properties prop = new Properties();
        InputStream input = null;
        Connection con = null;

        try {

            input = new FileInputStream("./src/main/resources/configuration.properties");

            // load a properties file
            prop.load(input);

            try {
                con = DriverManager.getConnection(
                        prop.getProperty("url"),
                        prop.getProperty("username"),
                        prop.getProperty("password")
                );
                con.setAutoCommit(false);

            } catch (SQLException ex) {

                Logger lgr = Logger.getLogger(DBManager.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
                throw new DBConnectionFailedException();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return con;
    }

    public enum QueryType {
        SELECT, INSERT;
    }
}
