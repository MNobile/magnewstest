Progetto di Test per MagNews

DESCRIZIONE PROGRAMMA
Il test realizzato è quello relativo a "DB Benchmarks". L'applicativo è molto semplice (non essendo i requisiti troppo specifici i ho seguiti
alla lettera per quanto possibile). Il benchmark è stato realizzato attraverso il lancio (seriale, non parallelizzato) di un certo numero
di INSERT/SELECT preimpostato (modificabile da file di configurazione) e relativa raccolta dei tempi.
Da notare che:
    - I tempi sono stati presi tramite l'oggetto System, questo significa che tutte le misurazioni comprendono anche i tempi di rete eventuali
    (aspetto non specificato da requisiti).
    - Il numero di query DML (in questo caso INSERT) denominato da requisito come X per la frequenza di commit è parametrizzabile da file
    di configurazione.
    -I test svolti sono relativi al singleton DBManager, in quanto non menzionati da requisiti ho sviluppato quelli che ho ritenuto
    significativi.
    -lo script per distruggere e ricreare la tabella di test è nella cartella delle configurazioni: dbInit.sql.
    -Non essendo specificato il tipo di GUI da impiegare, per semplicità i risultati del benchmark vengono scritti in linea di comando
    (in millisecondi).

LANCIO PROGRAMMA
Build + test : mvn clean package
Esecuzione: mvn exec:java


Autore: Marco Nobile